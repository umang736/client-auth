package com.websystique.springmvc.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.websystique.springmvc.Mapper.UserMapper;
import com.websystique.springmvc.model.User;

@Service("userService")
public class UserServiceImpl implements UserService {

	// private static final AtomicLong counter = new AtomicLong();
	// counter.incrementAndGet()
	private static JdbcTemplate template;

	static {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		template = new JdbcTemplate(context.getBean("dataSource", DataSource.class));
	}

	public List<User> findAllUsers() {
		return populateUsers();
	}

	public User findById(long id) {
		String SQL = "select * from clients where id = ?";
		try {
			User user = template.queryForObject(SQL, new Object[] { id }, new UserMapper());
			return user;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public void saveUser(User user) {
		String SQL = "insert into clients (id, name, age, salary) values (?, ?, ?, ?)";
		template.update(SQL, user.getId(), user.getName(), user.getAge(), user.getSalary());
	}

	public int updateUser(User user) {
		String SQL = "update clients set name=?, age=?, salary=? where id=?";
		int rows = template.update(SQL, user.getName(), user.getAge(), user.getSalary(), user.getId());
		return rows;
	}

	public int deleteUserById(long id) {
		String SQL = "delete from clients where id=?";
		int rows =template.update(SQL, id);
		return rows;
	}

	public boolean isUserExist(User user) {
		return findById(user.getId()) != null;
	}

	public void deleteAllUsers() {
		String SQL = "delete from clients";
		template.update(SQL);
	}

	private List<User> populateUsers() {
		// List<User> users = new ArrayList<User>();
		String SQL = "select * from clients";
		List<User> users = template.query(SQL, new UserMapper());
		/*
		 * users.add(new User(counter.incrementAndGet(),"umang",21, 70000));
		 * users.add(new User(counter.incrementAndGet(),"monu",23, 50000));
		 * users.add(new User(counter.incrementAndGet(),"ishu",22, 30000));
		 * users.add(new User(counter.incrementAndGet(),"rashi",19, 20000));
		 */return users;
	}

}
