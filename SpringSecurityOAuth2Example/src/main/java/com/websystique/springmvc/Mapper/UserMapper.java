package com.websystique.springmvc.Mapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.websystique.springmvc.model.User;

public class UserMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		User user = new User(rs.getLong("id"),rs.getString("name"),rs.getInt("age"),rs.getDouble("salary"));
	      return user;
	}

}
