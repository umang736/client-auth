
<!DOCTYPE>
<html >
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Basic App</title>
	<link rel="stylesheet" type="text/css"
	href="static/css/bootstrap.min.css">
	<style>
		.full-width{
			width:100%;
		}
		body{
			padding-top:70px;
		}
	</style>
</head>
<body ng-app="myFancyApp">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" >
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icom-bar"></span>
				</button>
				<a class="navbar-brand" href="#/">Queries Portal</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li><a href="#Queries">Queries</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li ng-show="islogged==false"><a href="#Signup"><span class="glyphicon glyphicon-user"></span>Sign Up</a></li>
					<li ng-show="islogged==false"><a href="#Login"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
					<li ng-show="islogged==true" ng-controller="LogoutCtrl" ng-click="logout()"><a href="#"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div ng-view class="col-sm-offset-1 col-md-offset-2 col-lg-offset-2 col-sm-7 col-md-6 col-lg-6"></div>
		
	</div>
	<!--<a href="static/html/Login.html">Go to Login</a>-->
</body>
<script type="text/javascript" src="static/js/angular.min.js"></script>
<script type="text/javascript" src="static/js/angular-route.min.js"></script>
<script type="text/javascript" src="static/js/ui-bootstrap-tpls.js"></script>
<script type="text/javascript" src="static/js/angular-script.js"></script>

</html>