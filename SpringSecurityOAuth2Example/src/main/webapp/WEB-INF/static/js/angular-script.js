var app=angular.module('myFancyApp', ['ui.bootstrap','ngRoute']).run(function($rootScope) {
    $rootScope.islogged = false;
});
app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
}]);// OR add ! after # in href urls(new in v1.6)
app.constant("GrantN","password");
app.constant("GrantR","refresh_token");
app.constant("idsecret","my-trusted-client:secret");

app.service('startsWith', function(){
    this.myFun = function (str,pre) {
        return (str.indexOf(pre) == 0);
    }
});

app.factory('token_data', ['GrantR','idsecret','startsWith','$http','$httpParamSerializer', function(GrantR,idsecret,startsWith,$http,$httpParamSerializer){
    var data={};
    var setToken = function(newObj) {
        console.log(newObj);
      data=newObj;
  };
  
  var getToken = function() {
    return data;
};

var refToken = function(){
   var form_data={
     grant_type:GrantR,
     refresh_token:data.refresh_token
 };

 var auth='Basic '+window.btoa(idsecret);
 return $http({
    method : "POST",
    url : "oauth/token",
    data: $httpParamSerializer(form_data),
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded' ,
    'Authorization': auth,//'Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0'
    'Accept': 'application/json' 
}
}).then(function mySucces(response) {
        //success callback
        var tok_data={};
        tok_data.access_token = response.data.access_token;
        tok_data.token_type= response.data.token_type;
        tok_data.refresh_token = response.data.refresh_token;
        tok_data.expires_in= response.data.expires_in;
        //console.log(tok_data);
        setToken(tok_data);
        console.log(getToken());
        return true;
    }, function myError(response) {
        //error callback
        var error_msg;
        console.log(response.statusText+response.data.error+response.data.error_description+response.status);
        if(response.data.error=='unsupported_grant_type')
            error_msg =response.data.error_description;
        else if(startsWith.myFun(response.data.error_description,'Invalid refresh token'))
            error_msg ='Invalid refresh token';
        return error_msg;
    });
    };

    return{
        getToken:  getToken, 
        refToken:  refToken,
        setToken:  setToken
    };
}]);

            app.config(function($routeProvider) {
             $routeProvider
             .when("/", {
              templateUrl : "static/html/Home.html"
          })
             .when("/Queries", {
              templateUrl : "static/html/Queries.html",
              controller : "QueriesCtrl"
          })
             .when("/Signup", {
              templateUrl : "static/html/SignUp.html"
          })
             .when("/Login", {
              templateUrl : "static/html/Login.html",
              controller : "LoginCtrl"
          })
             .otherwise({
              templateUrl : "static/html/404.html"
          });
         });

        app.controller("LogoutCtrl", function($scope,$rootScope,$location){
            $scope.logout=function(){
                $rootScope.islogged=false;
                $location.path('/');
            };
        });

        app.controller("LoginCtrl", function ($scope,$http,$location,$httpParamSerializer,GrantN,idsecret,token_data,startsWith,$rootScope) {

            $scope.validateCredentials = function () {
                console.log($scope.user+" "+$scope.pass);
                var form_data={
                 grant_type:GrantN,
                 username:$scope.user,
                 password:$scope.pass
             };

             var auth='Basic '+window.btoa(idsecret);
             $http({
                method : "POST",
                url : "oauth/token",
                data: $httpParamSerializer(form_data),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded' ,
    'Authorization': auth,//'Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0'
    'Accept': 'application/json' 
}
}).then(function mySucces(response) {
        //success callback
        var tok_data={};
        tok_data.access_token = response.data.access_token;
        tok_data.token_type= response.data.token_type;
        tok_data.refresh_token = response.data.refresh_token;
        tok_data.expires_in= response.data.expires_in;
       // console.log(tok_data);
       token_data.setToken(tok_data);
       console.log(token_data.getToken());
       $scope.success_msg='Login Successful';
       $rootScope.islogged=true;
       $location.path('/Queries');  
   }, function myError(response) {
        //error callback
        console.log(response.statusText+response.data.error+response.data.error_description+response.status);
        if(response.data.error_description=='Bad credentials')
            $scope.error_msg ="Invalid username or password";
        else if(response.data.error=='unsupported_grant_type')
            $scope.error_msg =response.data.error_description;
    });

};

});

        app.controller("QueriesCtrl", function ($scope,$http,startsWith,token_data,$rootScope) {
            $scope.types=['Retrieve All Clients','Retrieve a Client','Create a Client','Update a Client','Delete a Client','Delete all Clients'];
            $scope.query_type=1;

            $scope.feedback_class='text-danger';
            if($rootScope.islogged==false)
                $scope.feedback='Please log in to do Queries';
            else
               $scope.feedback='';

           $scope.config = {
            headers : {
                'Accept': 'application/json'
            }
        };

        $scope.updateFeedback=function(msg='',classType='text-danger'){
            $scope.feedback=msg;$scope.feedback_class=classType;
        }

        $scope.accessDenied=function(resp)
        {
         console.log(resp.statusText+resp.status);
         $scope.updateFeedback("Oops! You don't have access for making that request");
     }

      $scope.invalidToken=function(msg)
        {
         console.log(msg);
         $scope.updateFeedback("Please login again to continue");
     }
     $scope.setEmpty=function(query_type){
       // console.log(query_type);
        if(query_type==1)
            $scope.users=[];
        else if(query_type==2)
            $scope.singleClient={};     
     }

     $scope.getUsers=function(){
        
        $http.get('user/?access_token='+token_data.getToken().access_token, $scope.config).
        then(function(response){//success callback
            $scope.users=response.data;
            $scope.updateFeedback();
            } ,function(response){//error callback
                if(startsWith.myFun(response.data.error_description,'Access token expired'))
                {
                    //$scope.error_msg ='Access token expired';
                    var refreshed=token_data.refToken();
                    refreshed.then(function(result) {
                        if(result==true)
                            $scope.getUsers();
                        else
                            $scope.updateFeedback('Please login again to continue');
                        console.log("result="+result); 
                    }); 
                }
                else if(startsWith.myFun(response.data.error_description,'Invalid access token'))
                {
                    $scope.invalidToken(response.data.error_description);
                }
            });
    };

    $scope.q2={};
    $scope.getUserDetails=function(){
       //console.log($scope.q2.singleClient);
       $http.get('user/'+$scope.q2.singleClient+'?access_token='+token_data.getToken().access_token, $scope.config).
        then(function(response){//success callback
            $scope.singleClient=response.data;
            $scope.updateFeedback();
            } ,function(response){//error callback
              if(response.status==404)
              {
                  console.log(response.statusText+response.status);
                  $scope.q2.singleClient={};
                  $scope.updateFeedback('No such client exists');   
              }
              else if(startsWith.myFun(response.data.error_description,'Access token expired'))
              {
                    //$scope.error_msg ='Access token expired';
                    var refreshed=token_data.refToken();
                    refreshed.then(function(result) {
                        if(result==true)
                            $scope.getUserDetails();
                        else
                            {$scope.updateFeedback('Please login again to continue'); $scope.singleClient={};}
                        console.log("result="+result); 
                    }); 
                }
                else if(startsWith.myFun(response.data.error_description,'Invalid access token'))
                {
                    $scope.q2.singleClient={};
                     $scope.invalidToken(response.data.error_description);
                }
                
            });
    };

    $scope.q3={};
    $scope.createClient=function(){
       var form_data={
           "id":$scope.q3.Id,
           "name":$scope.q3.Name,
           "age":$scope.q3.Age,
           "salary":$scope.q3.Salary
       };
       $http({
        method : "POST",
        url : "user/"+"?access_token="+token_data.getToken().access_token,
        data: form_data,
        headers: $scope.config.headers
    }).then(function mySucces(response) {
        //success callback
        console.log(response.statusText+response.status);
        if(response.status==201)
          $scope.updateFeedback('New client added','text-success');
      else
        $scope.updateFeedback('Some error occurred, Please try again');
}, function myError(response) {
        //error callback
        if(response.status==403)
            $scope.accessDenied(response);
        else if(response.status==409)
        {
          console.log('Duplicate Entry');
          $scope.updateFeedback('A Client with Id '+$scope.q3.Id+' already exists');   
      }
      else if(startsWith.myFun(response.data.error_description,'Access token expired'))
      {
                    //$scope.error_msg ='Access token expired';
                    var refreshed=token_data.refToken();
                    refreshed.then(function(result) {
                        if(result==true)
                            $scope.createClient();
                        else
                            $scope.updateFeedback('Please login again to continue');
                        console.log("result="+result); 
                    }); 
                }
                else if(startsWith.myFun(response.data.error_description,'Invalid access token'))
                {
                     $scope.invalidToken(response.data.error_description);
                }
            });

};

$scope.q4={};
$scope.updateClient=function(){
   var form_data={
       "id":$scope.q4.Id,
       "name":$scope.q4.Name,
       "age":$scope.q4.Age,
       "salary":$scope.q4.Salary
   };
   $http({
    method : "PUT",
    url : "user/"+$scope.q4.Id+"?access_token="+token_data.getToken().access_token,
    data: form_data,
    headers: $scope.config.headers
}).then(function mySucces(response) {
        //success callback
        console.log(response.statusText+response.status);
        if(response.status==204)
          $scope.updateFeedback('Client updated','text-success');
      else
        $scope.updateFeedback('Some error occurred, Please try again');
}, function myError(response) {
        //error callback
        if(response.status==403)
            $scope.accessDenied(response);
        else if(response.status==404)
        {
          console.log(response.statusText+response.status);
          $scope.updateFeedback('No such client exists');   
      }
      else if(startsWith.myFun(response.data.error_description,'Access token expired'))
      {
                    //$scope.error_msg ='Access token expired';
                    var refreshed=token_data.refToken();
                    refreshed.then(function(result) {
                        if(result==true)
                            $scope.updateClient();
                        else
                            $scope.updateFeedback('Please login again to continue');
                        console.log("result="+result); 
                    }); 
                }
                else if(startsWith.myFun(response.data.error_description,'Invalid access token'))
                {
                     $scope.invalidToken(response.data.error_description);
                }
            });

};

$scope.q5={};
$scope.deleteClient=function(){
   $http({
    method : "DELETE",
    url : "user/"+$scope.q5.removeClient+"?access_token="+token_data.getToken().access_token,
    headers: $scope.config.headers
}).then(function mySucces(response) {
        //success callback
        console.log(response.statusText+response.status);        
        if(response.status==204)
          $scope.updateFeedback('Client deleted','text-success');
      else
        $scope.updateFeedback('Some error occurred, Please try again');
}, function myError(response) {
        //error callback
        if(response.status==403)
            $scope.accessDenied(response);
        else if(response.status==404)
        {
          console.log(response.statusText+response.status);
          $scope.updateFeedback('No such client exists');   
      }
      else if(startsWith.myFun(response.data.error_description,'Access token expired'))
      {
                    //$scope.error_msg ='Access token expired';
                    var refreshed=token_data.refToken();
                    refreshed.then(function(result) {
                        if(result==true)
                            $scope.deleteClient();
                        else
                            $scope.updateFeedback('Please login again to continue');
                        console.log("result="+result); 
                    }); 
                }
                else if(startsWith.myFun(response.data.error_description,'Invalid access token'))
                {
                     $scope.invalidToken(response.data.error_description);
                }
            });

};

$scope.deleteAllClients=function(){
   $http({
    method : "DELETE",
    url : "user/"+"?access_token="+token_data.getToken().access_token,
    headers: $scope.config.headers
}).then(function mySucces(response) {
        //success callback
        console.log(response.statusText+response.status);        
        if(response.status==204)
          $scope.updateFeedback('All clients deleted','text-success');
      else
        $scope.updateFeedback('Some error occurred, Please try again');
}, function myError(response) {
        //error callback
        if(response.status==403)
            $scope.accessDenied(response);
        else if(startsWith.myFun(response.data.error_description,'Access token expired'))
        {
                    //$scope.error_msg ='Access token expired';
                    var refreshed=token_data.refToken();
                    refreshed.then(function(result) {
                        if(result==true)
                            $scope.deleteAllClients();
                        else
                            $scope.updateFeedback('Please login again to continue');
                        console.log("result="+result); 
                    }); 
                }
                else if(startsWith.myFun(response.data.error_description,'Invalid access token'))
                {
                    $scope.invalidToken(response.data.error_description);
                }
            });

};

});