package admin.hr.employee.configuration;

import java.util.Properties;

import javax.sql.DataSource;
import javax.transaction.Transaction;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "admin.hr.employee.configuration" })
@PropertySource(value = { "classpath:application.properties" })
public class HibernateResourceConfiguration {
	 @Autowired
	    private Environment environment;
	 
	 	@Primary
	    @Bean(name="resourcesessionFactory")
	    public LocalSessionFactoryBean resourcesessionFactory() {
	        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	        sessionFactory.setDataSource(resourcedataSource());
	        sessionFactory.setPackagesToScan(new String[] { "admin.hr.employee.model" });
	        sessionFactory.setHibernateProperties(hibernateProperties());
	        return sessionFactory;
	 	}
	     
	    @Primary
	    @Bean(name="resourcedataSource")
	    public DataSource resourcedataSource() {
	        DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
	        dataSource.setUrl(environment.getRequiredProperty("jdbcresource.url"));
	        dataSource.setUsername(environment.getRequiredProperty("jdbcresource.username"));
	        dataSource.setPassword(environment.getRequiredProperty("jdbcresource.password"));
	        return dataSource;
	    }
	     
	    private Properties hibernateProperties() {
	        Properties properties = new Properties();
	        properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
	       // hibernate.hbm2ddl.auto=update
	        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
	        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
	        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
	        return properties;        
	    }
	    
	    @Primary
	    @Bean(name="resourceTransactionManager")
	    @Autowired
	    public HibernateTransactionManager authTransactionManager(@Qualifier("authsessionFactory") SessionFactory s) {
	       HibernateTransactionManager txManager = new HibernateTransactionManager();
	       txManager.setSessionFactory(s);
	       return txManager;
	    
	    }

}
