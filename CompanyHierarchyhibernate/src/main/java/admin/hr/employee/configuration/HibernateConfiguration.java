package admin.hr.employee.configuration;

import java.util.Properties;

import javax.sql.DataSource;
import javax.transaction.Transaction;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

//uncomment to use one config file instead of two
/*
@Configuration
@EnableTransactionManagement//-> gives error for using 2 transacn mamnagers with no main/primary manager
@ComponentScan({ "admin.hr.employee.configuration" })
@PropertySource(value = { "classpath:application.properties" })
*/
public class HibernateConfiguration {
	/* @Autowired
	    private Environment environment;
	 
	 	@Primary
	    @Bean(name="resourcesessionFactory")
	    public LocalSessionFactoryBean resourcesessionFactory() {
	        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	        sessionFactory.setDataSource(resourcedataSource());
	        sessionFactory.setPackagesToScan(new String[] { "admin.hr.employee.model" });
	        sessionFactory.setHibernateProperties(hibernateProperties());
	        return sessionFactory;
	     }
	    
	    @Bean(name="authsessionFactory")
	    public LocalSessionFactoryBean authsessionFactory() {
	        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	        sessionFactory.setDataSource(authdataSource());
	        sessionFactory.setPackagesToScan(new String[] { "admin.hr.employee.model" });
	        sessionFactory.setHibernateProperties(hibernateProperties());
	        return sessionFactory;
	     }
	     
	    @Primary
	    @Bean(name="resourcedataSource")
	    public DataSource resourcedataSource() {
	        DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
	        dataSource.setUrl(environment.getRequiredProperty("jdbcresource.url"));
	        dataSource.setUsername(environment.getRequiredProperty("jdbcresource.username"));
	        dataSource.setPassword(environment.getRequiredProperty("jdbcresource.password"));
	        return dataSource;
	    }
	    
	    @Bean(name="authdataSource")
	    public DataSource authdataSource() {
	        DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
	        dataSource.setUrl(environment.getRequiredProperty("jdbcauth.url"));
	        dataSource.setUsername(environment.getRequiredProperty("jdbcauth.username"));
	        dataSource.setPassword(environment.getRequiredProperty("jdbcauth.password"));
	        return dataSource;
	    }
	     
	    private Properties hibernateProperties() {
	        Properties properties = new Properties();
	        properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
	       // hibernate.hbm2ddl.auto=update
	        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
	        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
	        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
	        return properties;        
	    }
	    
	    
	   @Bean(name="authTransactionManager")
	    @Autowired
	    public HibernateTransactionManager resourceTransactionManager( @Qualifier("resourcesessionFactory") SessionFactory s) {
	       HibernateTransactionManager txManager = new HibernateTransactionManager();
	       txManager.setSessionFactory(s);
	       return txManager;
	    }
	   
	    @Primary
	    @Bean(name="resourceTransactionManager")
	    @Autowired
	    public HibernateTransactionManager authTransactionManager(@Qualifier("authsessionFactory") SessionFactory s) {
	       HibernateTransactionManager txManager = new HibernateTransactionManager();
	       txManager.setSessionFactory(s);
	       return txManager;
	    
	    }
*/
}
