package admin.hr.employee.mapper;

import java.util.ArrayList;
import java.util.List;

import admin.hr.employee.model.User;
import admin.hr.employee.transfer.Tuser;

public class UserConverter {
	public Tuser getTuser(User u){
		return new Tuser(u.getId(),u.getName(),u.getAge(),u.getType(),u.getDept().getId());
	}
	
	public List<Tuser> getTusers(List<User> users){
		List<Tuser> tusers = new ArrayList<Tuser>();
		for (User u : users) {
			tusers.add(getTuser(u));
			// System.out.println(u);
		}
		return tusers;
	}

}
