package admin.hr.employee.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import admin.hr.employee.mapper.UserConverter;
import admin.hr.employee.model.Counter;
import admin.hr.employee.model.Department;
import admin.hr.employee.model.User;
import admin.hr.employee.transfer.Tuser;

@Repository("resourceDao")
public class ResourceDaoImpl extends ResourceAbstractDao implements ResourceDao {

	private static UserConverter um;
	static {
		um = new UserConverter();
	}
	
	public Tuser findById(String id) {
		try {
			Criteria criteria = getSession().createCriteria(User.class);
			criteria.add(Restrictions.eq("id", id));
			User u = (User) criteria.uniqueResult();
			return um.getTuser(u);
		} catch (HibernateException e) {
			return null;
		}
	}

	public void saveUser(Tuser tuser) {
		// TODO Auto-generated method stub
		Criteria criteria = getSession().createCriteria(Counter.class);
		Counter c = (Counter) criteria.uniqueResult();
		StringBuilder Empid = new StringBuilder((String) c.getPrefix());
		int count = (Integer) c.getCount();
		count = count + 1;
		if (count < 10)
			Empid.append("00");
		else if (count < 100)
			Empid.append('0');
		Empid.append(count);
		Department dept = findByDeptId(tuser.getDeptid());
		User u = new User(tuser.getName(), tuser.getAge(), tuser.getType(), dept);
		u.setId(Empid.toString());
		System.out.println("user: " + u + " ,counter: " + c);
		persist(u);
		c.setCount(count);
		getSession().update(c);
	}

	public int updateUser(Tuser tuser) {
		try {
			Department dept = findByDeptId(tuser.getDeptid());
			User u = new User(tuser.getName(), tuser.getAge(), tuser.getType(), dept);
			u.setId(tuser.getId());
			getSession().update(u);
			return 1;
		} catch (HibernateException e) {
			return 0;
		}

	}

	public int deleteUserById(String userId) {
		//try {
			Query query = getSession().createQuery("delete from User where id = :id");
			query.setString("id", userId);
			int rowDeleted=query.executeUpdate();
			return rowDeleted;
	/*	} catch (HibernateException e) {
			return 0;
		}*/
	}

	@SuppressWarnings("unchecked")
	public List<Tuser> findAllUsers() {
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.ne("id", "Admin736"));
		List<User> userls = (List<User>) criteria.list();
		List<Tuser> tuserls = um.getTusers(userls);
		return tuserls;

	}

	public void deleteAllUsers() {
		Query query = getSession().createQuery("delete from user");
		query.executeUpdate();

	}
	
	@SuppressWarnings("unchecked")
	public List<Department> findAllDepartments() {
		Criteria criteria = getSession().createCriteria(Department.class);
		criteria.add(Restrictions.ne("id", "HD"));
		return (List<Department>) criteria.list();
	}

	public boolean isDepartmentExist(Department dept) {
		return findByDeptId(dept.getId()) != null;
	}

	public Department findByDeptId(String id) {
		try {
			Criteria criteria = getSession().createCriteria(Department.class);
			criteria.add(Restrictions.eq("id", id));
			return (Department) criteria.uniqueResult();
		} catch (HibernateException e) {
			return null;
		}

	}

	public void saveDepartment(Department dept) {
		persist(dept);

	}

	public int updateDepartment(Department dept) {
		try {
			getSession().update(dept);
			return 1;
		} catch (HibernateException e) {
			return 0;
		}

	}

	public int deleteDepartmentById(String deptId) {
		//try {
			Query query = getSession().createQuery("delete from Department where id = :id");
			query.setString("id", deptId);
			int rowDeleted=query.executeUpdate();
			return rowDeleted;
		/*} catch (HibernateException e) {
			return 0;
		}*/
	}

	@SuppressWarnings("unchecked")
	public List<Tuser> findUsersByDeptid(String deptId) {
		// String SQL = "select * from user where deptid = ?";
		try {
			Criteria usercrit = getSession().createCriteria(User.class);
			usercrit.add(Restrictions.eq("dept.id", deptId));
			List<User> userls = (List<User>) usercrit.list();
			List<Tuser> tuserls = um.getTusers(userls);
			return tuserls;
		} catch (HibernateException e) {
			return null;
		}
	}

}
