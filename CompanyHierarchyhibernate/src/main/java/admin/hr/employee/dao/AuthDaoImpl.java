package admin.hr.employee.dao;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository("authDao")
public class AuthDaoImpl extends AuthAbstractDao implements AuthDao{
	public String findIdByUserame(String username){
		
		Query query = getSession().createSQLQuery("select id from credentials where username = :username");
		query.setString("username",username);
        return (String) query.uniqueResult();
	}
}
