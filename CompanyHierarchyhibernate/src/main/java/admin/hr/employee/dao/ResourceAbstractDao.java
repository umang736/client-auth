package admin.hr.employee.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class ResourceAbstractDao {
	 @Autowired
	 @Qualifier("resourcesessionFactory")
	    private SessionFactory sessionFactory;
	 
	    protected Session getSession() {
	    	return sessionFactory.getCurrentSession();
	    }
	 
	    public void persist(Object entity) {
	        getSession().persist(entity);
	    }
	 
	    public void delete(Object entity) {
	        getSession().delete(entity);
	    }
}
