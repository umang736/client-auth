package admin.hr.employee.transfer;

public class Tuser {

		private String id;
		
		private String name;
		
		private int age;
		
		private String type;
		
		private String deptid;

		public Tuser(){
			id=null;
		}
		
		public Tuser(String id, String name, int age, String type, String deptid){
			this.id = id;
			this.name = name;
			this.age = age;
			this.type = type;
			this.deptid=deptid;
		}
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		
		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDeptid() {
			return deptid;
		}

		public void setDeptid(String deptid) {
			this.deptid = deptid;
		}

		@Override
		public String toString() {
			return "{id:" + id + ", name:" + name + ", age:" + age + ", type:" + type + ", deptid:" + deptid + "}";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Tuser other = (Tuser) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}
		
}
