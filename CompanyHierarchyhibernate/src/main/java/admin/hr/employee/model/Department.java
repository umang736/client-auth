package admin.hr.employee.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})/*
entities are loaded lazy and serialization happens before they get loaded fully
*/
@Table(name="department", uniqueConstraints = {
		@UniqueConstraint(columnNames = "deptid")})
public class Department {
	@Id
	@Column(name = "deptid",length=10,unique=true,nullable=false)
	private String id;
	@Column(name = "name",length=40,nullable=false)
	private String name;
	@Column(name = "description",length=50,nullable=false)
	private String description;

	@OneToMany(cascade = CascadeType.ALL,mappedBy="dept")
	@Fetch(FetchMode.SELECT)
	//@JsonBackReference//is the back part of reference � it will be omitted from serialization.
	@JsonIgnore
	private Set<User> users= new HashSet<User>(0);
	
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(HashSet<User> users) {
		this.users = users;
	}

	public Department(){
		id=null;
	}
	public Department(String id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "{id=" + id + ", name=" + name + ", description=" + description + "}";
	}
}
