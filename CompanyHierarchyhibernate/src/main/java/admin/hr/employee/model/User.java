package admin.hr.employee.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="user",uniqueConstraints = {
		@UniqueConstraint(columnNames = "id")})
public class User {

	@Id
	@Column(name = "id",unique=true,nullable=false)
	private String id;
	
	@Column(name = "name",nullable=false)
	private String name;
	
	@Column(name = "age",nullable=false)
	private int age;
	
	@Column(name = "type",length=10,nullable=false)
	private String type;
	
	@ManyToOne(fetch=FetchType.EAGER)//default fetch type is FETCHTYPE.LAZY
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "deptid",nullable=false)
	//@JsonManagedReference//is the forward part of reference � the one that gets serialized normally.
	private Department dept;

	public User(){
		id=null;
	}
	
	public User(String name, int age, String type, Department dept){
		this.name = name;
		this.age = age;
		this.type = type;
		this.dept = dept;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getDept() {
		return dept;
	}

	public void setDept(Department dept) {
		this.dept = dept;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "{id:" + id + ", name:" + name + ", age:" + age + ", type:" + type + ", deptid:" + dept.getId() + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
