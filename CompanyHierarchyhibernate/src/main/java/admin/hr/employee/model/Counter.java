package admin.hr.employee.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="counter")
public class Counter {
	@Id
	@Column(name = "prefix",nullable=false)
	private String prefix;
	@Column(name = "count",nullable=false)
	private int count;
	public Counter() {
		super();
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "Counter [prefix=" + prefix + ", count=" + count + "]";
	}
	
}
