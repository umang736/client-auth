package admin.hr.employee.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="credentials")
public class Credentials implements Serializable{
	
	@Id
	@OneToOne
    @JoinColumn(name="id")
	private User user;
	
	@Column(name = "username",length=15,nullable=false)
	private String username;
	
	@Column(name = "password",length=20,nullable=false)
	private String password;
	
	@Column(name = "enabled",length=1,nullable=false)
	private Byte enabled;
	
	public Credentials() {
		 
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Byte getEnabled() {
		return enabled;
	}

	public void setEnabled(Byte enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "Credentials [id=" + user.getId() + ", username=" + username + ", password=" + password + ", enabled="
				+ enabled + "]";
	}
	
}
