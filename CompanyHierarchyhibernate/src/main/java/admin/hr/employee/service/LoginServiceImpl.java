package admin.hr.employee.service;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import admin.hr.employee.dao.AuthDao;

@Service("loginService")
@Transactional("authTransactionManager")
public class LoginServiceImpl implements LoginService {
		
	 @Autowired
	    private AuthDao dao;
	public String findIdByUserame(String username) {
		return dao.findIdByUserame(username);
	}

}
