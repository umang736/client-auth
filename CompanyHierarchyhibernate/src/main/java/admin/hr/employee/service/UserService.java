package admin.hr.employee.service;

import java.util.List;

import admin.hr.employee.model.Department;
import admin.hr.employee.model.User;
import admin.hr.employee.transfer.Tuser;

public interface UserService {
	
	Tuser findById(String id);
	
	void saveUser(Tuser user);
	
	int updateUser(Tuser user);
	
	int deleteUserById(String id);

	List<Tuser> findAllUsers(); 
	
	void deleteAllUsers();

	List<Department> findAllDepartments();

	public boolean isDepartmentExist(Department dept);

	void saveDepartment(Department dept);

	int updateDepartment(Department dept);

	int deleteDepartmentById(String id);

	List<Tuser> findUsersByDeptid(String deptid);
	
}
