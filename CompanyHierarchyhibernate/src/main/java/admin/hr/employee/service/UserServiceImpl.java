package admin.hr.employee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import admin.hr.employee.dao.ResourceDao;
import admin.hr.employee.model.Department;
import admin.hr.employee.model.User;
import admin.hr.employee.transfer.Tuser;

@Service("userService")
@Transactional("resourceTransactionManager")
public class UserServiceImpl implements UserService {

	 @Autowired
	    private ResourceDao dao;
	 
	public List<Tuser> findAllUsers() {
		return dao.findAllUsers();
	}
	
	public List<Tuser> findUsersByDeptid(String deptid) {
		return dao.findUsersByDeptid(deptid);
	}
	
	public Tuser findById(String id) {
		return dao.findById(id);
	}

	public void saveUser(Tuser user) {
		dao.saveUser(user);
		
	}

	public int updateUser(Tuser user) {
		return dao.updateUser(user);
	}
	
	public int updateDepartment(Department dept) {
		return dao.updateDepartment(dept);
	}
	
	public int deleteDepartmentById(String id) {
		return dao.deleteDepartmentById(id);
	}

	public int deleteUserById(String id) {
		return dao.deleteUserById(id);
	}

	public List<Department> findAllDepartments() {
		return dao.findAllDepartments();
	}

	public boolean isDepartmentExist(Department dept) {
		return dao.isDepartmentExist(dept);
	}

	public Department findByDeptId(String id) {
		return dao.findByDeptId(id);
	}
	
	public void saveDepartment(Department dept) {
		dao.saveDepartment(dept);
		
	}

	public void deleteAllUsers() {
		dao.deleteAllUsers();
	}

}
