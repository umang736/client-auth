package admin.hr.employee.Mapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import admin.hr.employee.model.Department;

public class DepartmentMapper implements RowMapper<Department> {

	public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Department dept = new Department(rs.getString("id"),rs.getString("name"),rs.getString("description"));
	      return dept;
	}

}
