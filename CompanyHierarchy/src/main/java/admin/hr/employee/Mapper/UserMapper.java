package admin.hr.employee.Mapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import admin.hr.employee.model.User;

public class UserMapper implements RowMapper<User> {

	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		User user = new User(rs.getString("id"),rs.getString("name"),rs.getInt("age"),rs.getString("type"), rs.getString("deptid"));
	      return user;
	}

}
