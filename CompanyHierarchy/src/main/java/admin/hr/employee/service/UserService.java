package admin.hr.employee.service;

import java.util.List;

import admin.hr.employee.model.Department;
import admin.hr.employee.model.User;

public interface UserService {
	
	User findById(String id);
	
	void saveUser(User user);
	
	int updateUser(User user);
	
	int deleteUserById(String id);

	List<User> findAllUsers(); 
	
	void deleteAllUsers();

	List<Department> findAllDepartments();

	public boolean isDepartmentExist(Department dept);

	void saveDepartment(Department dept);

	int updateDepartment(Department dept);

	int deleteDepartmentById(String id);

	List<User> findUsersByDeptid(String deptid);
	
}
