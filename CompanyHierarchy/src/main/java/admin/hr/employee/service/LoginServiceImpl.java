package admin.hr.employee.service;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service("loginService")
public class LoginServiceImpl implements LoginService {
	
		private static JdbcTemplate template;

		static {
			ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
			template = new JdbcTemplate(context.getBean("authDataSource", DataSource.class));
		}
		
	public String findIdByUserame(String username) {
		String SQL = "select id from credentials where username = ?";
		try {
			String id = template.queryForObject(SQL, new Object[] { username }, String.class);
			return id;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

}
