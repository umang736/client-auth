package admin.hr.employee.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import admin.hr.employee.Mapper.DepartmentMapper;
import admin.hr.employee.Mapper.UserMapper;
import admin.hr.employee.model.Department;
import admin.hr.employee.model.User;

@Service("userService")
public class UserServiceImpl implements UserService {

	// private static final AtomicLong counter = new AtomicLong();
	// counter.incrementAndGet()
	private static JdbcTemplate template;

	static {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		template = new JdbcTemplate(context.getBean("resourceDataSource", DataSource.class));
	}

	public List<User> findAllUsers() {
		// List<User> users = new ArrayList<User>();
				String SQL = "select * from user";
				List<User> users = template.query(SQL, new UserMapper());
				return users;
	}
	
	public List<User> findUsersByDeptid(String deptid) {
		String SQL = "select * from user where deptid = ?";
		try {
			List<User> users = template.query(SQL, new Object[] { deptid }, new UserMapper());
			return users;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	public User findById(String id) {
		String SQL = "select * from user where id = ?";
		try {
			User user = template.queryForObject(SQL, new Object[] {id}, new UserMapper());
			return user;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public void saveUser(User user) {
		String SQL = "select prefix, count from counter";
        Map<String, Object> m=  template.queryForMap(SQL);
        StringBuilder Empid=new StringBuilder((String) m.get("prefix"));
        int count=(Integer) m.get("count");
       
        count=count+1;
        if(count<10)
        	Empid.append("00");
        else if(count<100)
        	Empid.append('0');	
        Empid.append(count);
       /* StringBuilder role=new StringBuilder("ROLE_");
        role.append(user.getType());*/
        SQL = "insert into user (id, name, age, type, deptid) values (?, ?, ?, ?, ?)";
		template.update(SQL, Empid.toString(), user.getName(), user.getAge(), user.getType(), user.getDeptid());
	
		SQL = "update counter set count=?";
		template.update(SQL, count);
		
	}

	public int updateUser(User user) {
		String SQL = "update user set name=?, age=?, type=?, deptid=? where id=?";
		int rows = template.update(SQL, user.getName(), user.getAge(), user.getType(), user.getDeptid(), user.getId());
		return rows;
	}
	
	public int updateDepartment(Department dept) {
		String SQL = "update department set name=?, description=? where id=?";
		int rows = template.update(SQL, dept.getName(), dept.getDescription(), dept.getId());
		return rows;
	}
	
	public int deleteDepartmentById(String id) {
		String SQL = "delete from department where id=?";
		int rows = template.update(SQL, id);
		return rows;
	}

	public int deleteUserById(String id) {
		String SQL = "delete from user where id=?";
		int rows = template.update(SQL, id);
		return rows;
	}

	public List<Department> findAllDepartments() {
		String SQL = "select * from department where id <> ?";
		List<Department> depts = template.query(SQL, new Object[] {"HD"}, new DepartmentMapper());//HD is admin dept. id
		for(Department d : depts)
			System.out.println(d);
		return depts;
	}

	public boolean isDepartmentExist(Department dept) {
		return findByDeptId(dept.getId()) != null;
	}

	private Department findByDeptId(String id) {
		String SQL = "select * from department where id = ?";
		try {
			Department dept = template.queryForObject(SQL, new Object[] { id }, new DepartmentMapper());
			return dept;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	public void saveDepartment(Department dept) {
		String SQL = "insert into department (id, name, description) values (?, ?, ?)";
		template.update(SQL, dept.getId(), dept.getName(), dept.getDescription());
		
	}

	public void deleteAllUsers() {
		String SQL = "delete from user";
		template.update(SQL);
	}

}
