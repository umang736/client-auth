package admin.hr.employee.controller;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import admin.hr.employee.model.Department;
import admin.hr.employee.model.User;
import admin.hr.employee.service.LoginService;
import admin.hr.employee.service.UserService;
 
@RestController
public class HelloWorldRestController {
 
    @Autowired
    UserService userService;  //Service which will do all data retrieval/manipulation work
    
    @Autowired
    LoginService loginService;  //Service which will do all data retrieval/manipulation work 
     
    //-------------------Retrieve All Users--------------------------------------------------------
     
    @RequestMapping(value = "/users/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() {
        List<User> users = userService.findAllUsers();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
    
    //-------------------Retrieve LoggedIn User Details--------------------------------------------------------
    
    @RequestMapping(value = "/user/{username}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Map<String,Object>> getUser(@PathVariable("username") String username) {
        System.out.println("Fetching User with username " + username);
        String id=loginService.findIdByUserame(username);
        User user = userService.findById(id);
        List<Department> depts = userService.findAllDepartments();
        Map<String,Object> m= new HashMap<String, Object>();
        if (user == null) {
            System.out.println("User with username " + username + " not found");
            m.put("user", null);
            m.put("dept", null);
            return new ResponseEntity<Map<String,Object>>(HttpStatus.NOT_FOUND);
        }
        
        m.put("user", user);	
        m.put("dept", depts);
        
        return new ResponseEntity<Map<String,Object>>(m, HttpStatus.OK);
    }
    
    
    //-------------------Retrieve All Departments--------------------------------------------------------
    
    @RequestMapping(value = "/dept/", method = RequestMethod.GET)
    public ResponseEntity<List<Department>> listAllDepartments() {
        List<Department> depts = userService.findAllDepartments();
        if(depts.isEmpty()){
            return new ResponseEntity<List<Department>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Department>>(depts, HttpStatus.OK);
    } 
 
 
    //-------------------Retrieve Users from Department--------------------------------------------------------
     
    @RequestMapping(value = "/users/{deptid}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<User>> getUsersFromDept(@PathVariable("deptid") String deptid) {
        System.out.println("Fetching Users from deptid " + deptid);
        List<User> users = userService.findUsersByDeptid(deptid);
        if (users == null) {
            System.out.println("Users from deptid " + deptid + " not found");
            return new ResponseEntity<List<User>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
 
     
     
    //-------------------Create a User--------------------------------------------------------
     
    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating User " + user.getName());
 
        userService.saveUser(user);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/users/{deptid}").buildAndExpand(user.getDeptid()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    //-------------------Create a Department--------------------------------------------------------
    
    @RequestMapping(value = "/dept/", method = RequestMethod.POST)
    public ResponseEntity<Void> createDepartment(@RequestBody Department dept, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Department " + dept.getName());
        System.out.println(dept.toString());
 
        if (userService.isDepartmentExist(dept)) {
            System.out.println("A Department with name " + dept.getName() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
 
        userService.saveDepartment(dept);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/dept/").build().toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
   
    //------------------- Update a User --------------------------------------------------------
     
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") String id, @RequestBody User user) {
        System.out.println("Updating User " + id);
         
        int updated=userService.updateUser(user);
         
        if (updated==0) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
 
        return new ResponseEntity<User>(user, HttpStatus.NO_CONTENT);
    }
    
    //------------------- Update a Department --------------------------------------------------------
    
    @RequestMapping(value = "/dept/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Department> updateDepartment(@PathVariable("id") String id, @RequestBody Department dept) {
        System.out.println("Updating User " + id);
         
        int updated=userService.updateDepartment(dept);
         
        if (updated==0) {
            System.out.println("Department with id " + id + " not found");
            return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
        }
 
        return new ResponseEntity<Department>(dept, HttpStatus.NO_CONTENT);
    }
 
    //------------------- Delete a User --------------------------------------------------------
     
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") String id) {
        System.out.println("Fetching & Deleting User with id " + id);
 
        int updated=userService.deleteUserById(id);
        if (updated == 0) {
            System.out.println("Unable to delete. User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
 
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
    
    //------------------- Delete a Department --------------------------------------------------------
    
    @RequestMapping(value = "/dept/{deptid}", method = RequestMethod.DELETE)
    public ResponseEntity<Department> deleteDepartment(@PathVariable("deptid") String deptid) {
        System.out.println("Fetching & Deleting Department with deptid " + deptid);
 
        int updated=userService.deleteDepartmentById(deptid);
        if (updated == 0) {
            System.out.println("Unable to delete. Department with deptid " + deptid + " not found");
            return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
        }
 
        return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
    }
 
    
    //------------------- Delete All Users --------------------------------------------------------
     
    @RequestMapping(value = "/users/", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteAllUsers() {
        System.out.println("Deleting All Users");
 
        userService.deleteAllUsers();
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
 
}