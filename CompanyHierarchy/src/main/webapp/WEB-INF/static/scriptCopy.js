console.log("ScriptCopy is loaded  !!!");
var app=angular.module("app",["ngRoute"]);
		angular.module("app").config(function($routeProvider) {
			$routeProvider
			.when("/loggedIn", {
				templateUrl : "static/loggedIn.html"
				// template : "Hiiiiiii"
			})
			.when("/", {
					templateUrl : "static/loginPage.html"
			});
		});

	angular.module("app").controller("myCtrl",function($scope){
			$scope.login=function(){
				console.log("In login process !!!");
			}
			$scope.name="Aashay";
	})
	angular.module("app").factory('Token', function(){
		return { access_token: '',
				 refresh_token: ''
		};
	});
	// shring of employee Type between controllers
	angular.module("app").factory('LoggedInEmployee', function(){
		return { user: {},
				 dept:{}
		};
	});
	
	
	
	
	angular.module("app").controller("loggedIn",['$scope','Token','$http','LoggedInEmployee',function($scope,Token,$http,LoggedInEmployee){	
		
		$scope.eType=true;
		//$scope.type='admin';
		$scope.department={};
		$scope.newEmployee={};
		console.log(LoggedInEmployee);
		$scope.LoggedInEmployee=LoggedInEmployee;
		$scope.employee=$scope.LoggedInEmployee.user;
		$scope.type=$scope.employee.type;
		$scope.departments=$scope.LoggedInEmployee.dept;
		//$scope.eType=EmployeeType.type;
		$scope.Token=Token;
		$scope.showUsers=false;
		// add features for add employee and department...
		// fetch all departments by calling the backend service service
		//$scope.departments=["Software","HR","QA","Support"];
		$scope.showBody=[];
		$scope.deptEmployees=[];
		$scope.deptEmployeesDisplay=[];
		for(var i=0;i<$scope.departments.length;i++){
			$scope.showBody[i]=false;
			$scope.deptEmployeesDisplay[i]=false;
		}
		//employees for different deparments..
		$scope.softwares=["Aashay","Umang","Aashish"];
		$scope.displayEmployees=[false,false,false,false];
		
		$scope.showEmployees=function(index){
			var url="users/"+$scope.departments[index].id +"?access_token="+$scope.Token.access_token;
			//if($scope.deptEmployees.length >= index  && $scope.deptEmployees[index].length <=0 ){
				$http.get(url).then(function successCallBack(res){
					console.log(res);
					$scope.deptEmployees[index]=res.data;
					$scope.deptEmployeesDisplay[index]=!$scope.deptEmployeesDisplay[index];
				},function errorCallBack(){
					
				})
			//}
			//else{
			//	$scope.deptEmployeesDisplay[index]=!$scope.deptEmployeesDisplay[index];
			//}
			
		}
		$scope.showDepartment=function(index){
			$scope.showBody[index]=!$scope.showBody[index];
		}
		$scope.insertEmployee=function(department){
			$scope.employee.department=department;
			console.log(department);
			var addEmployee={};
			addEmployee.name=$scope.newEmployee.username;
			addEmployee.age=$scope.newEmployee.age;
			addEmployee.type=$scope.newEmployee.type;
			addEmployee.deptid=department.id;
			addEmployee.id="";
			console.log(addEmployee);
			$scope.employee={};
			$scope.newEmployee={};
			//user/?access_token=d917ded9-e6c7-49b5-9a26-02f71538d2b8
			var url="user/?access_token="+$scope.Token.access_token;
			$http.post(url,addEmployee).then(function successsCallBack(res){
				console.log("added employee in "+ department.name);
			},function errorCallBack(err){
				console.log("Could not able to update the employee.");
			})
			
		}
		
		$scope.addDepartment=function(){
			console.log($scope.department);
			var url="dept/?access_token="+$scope.Token.access_token;
			var newDept={};
			newDept.id=$scope.department.newDepartmentId;
			newDept.name=$scope.department.newDepartmentName;
			newDept.description=$scope.department.newDepartmentDescription;
			console.log(newDept);
			$http.post(url,newDept).then(function successCallBack(){
				console.log("Added new department successfully !!!");
				$scope.departments.push(newDept);
				$scope.showBody[$scope.departments.length-1]=false;
				$scope.deptEmployeesDisplay[$scope.departments.length-1]=false
			},function errorCallBack(){
				console.log("Could not able to add new department");
			})
		}
	
	}])
	
	angular.module("app").controller("loginForm",['$scope','$location','$http','Token','LoggedInEmployee',function($scope,$location,$http,Token,LoggedInEmployee){
		$scope.badCredentials=false;	
		$scope.Token=Token;
		$scope.LoggedInEmployee=LoggedInEmployee;
		$scope.displaySpinner=false;
		$scope.login=function(){
			//	localhost:8011/CompanyHierarchy/oauth/token?grant_type=password&username=umang736&password=umang123
				//$scope.displaySpinner=true;
				//$('#mydiv').show();
				if($scope.userName==='aashay' && $scope.pwd==='abc123'){
						 var url="oauth/token?grant_type=password&username=umang736&password=umang123";
						// Encode the String
						var string = btoa($scope.userName+":"+$scope.pwd);
						// need to change later
						var auth=btoa("my-trusted-client:secret");
						console.log(auth);
						headers = {"Authorization": "Basic " + auth};
						
						
						
					     $http.post(url, {headers: headers}).then(function successCallback(response) {
						 console.log(response);
						 console.log(response.data.access_token);
						 $scope.Token.access_token=response.data.access_token;
						 $scope.Token.refresh_token=response.data.refresh_token;
						 var newUrl="user/umang736?access_token="+$scope.Token.access_token;
						 $http.get(newUrl).then(function successCallback(res){
						 		$scope.LoggedInEmployee.dept=res.data.dept;
						 		$scope.LoggedInEmployee.user=res.data.user;
						 		console.log(res);
						 		$location.path('\loggedIn');
						 },function errorCallback(){
							 console.log("Inside error function while fetching details......")
						 })
						 
					},function errorCallback(error){
						console.log("Inside error function of first url while fetching token......")
					}) 
				}
				else{
					$scope.badCredentials=true;
					$scope.userName="";
					$scope.pwd="";
					console.log("Credentials are not right !!!!");
				}
			}
	}])
	
	/* angular.module("app").controller("addEmployee",['$scope','$location','$http','Token',function($scope,$location,$http,Token){
		$scope.someTxt="SomeTxt !!!";
		$scope.employee={};
		$scope.insertEmployee=function(){
			console.log($scope.employee.name);
		}
	}])
	 */
	//$scope.displaySpinner=false;
						//$('#mydiv').hide();
						//$location.path('\loggedIn');
						
						
						// var object={
						//   "dept": [
						// 	{
						// 	  "id": "HR",
						// 	  "name": "Human Resource",
						// 	  "description": "employee management people come under this"
						// 	},
						// 	{
						// 	  "id": "SO",
						// 	  "name": "Software Engineering",
						// 	  "description": "tech guys come under this"
						// 	}
						//   ],
						//   "user": {
						// 	"id": "Admin736",
						// 	"name": "umang gupta",
						// 	"age": 21,
						// 	"type": "ROLE_Admin",
						// 	"deptid": "HD"
						//   }
						// }
							// $scope.LoggedInEmployee.dept=object.dept;
							// $scope.LoggedInEmployee.user=object.user;
							// console.log($scope.LoggedInEmployee);
							//  $location.path('\loggedIn');
	
	