var app=angular.module("app",["ngRoute"]);

	angular.module("app").controller("myCtrl",function($scope){
			$scope.login=function(){
				console.log("In login process !!!");
			}
	})
	angular.module("app").controller("login",['$scope','Token','$http',function($scope,Token,$http){	
		
		$scope.Token=Token;
		console.log($scope.Token.access_token);
		$scope.showUsers=false;
		$scope.getUsers=function(){
			console.log($scope.Token.access_token);	
			var url="http://localhost:8080/SpringSecurityOAuth2Example/user/?access_token="+$scope.Token.access_token;
			$http.get(url).then(function successCallback(response){
				console.log(response);
				$scope.users=response.data;
				$scope.showUsers=true;
			},function errorCallback(error){
				console.log("Inside error...")
				$scope.showUsers=false;
			});
			
		}

		$scope.refreshTokens=function(){
			var url="http://localhost:8080/SpringSecurityOAuth2Example/oauth/token?grant_type=refresh_token&refresh_token="+$scope.Token.refresh_token;
			console.log("Previous Tokens is : ");
			console.log($scope.Token);
			$http.post(url).then(function successCallback(response) {
				 console.log(response);
				 $scope.Token.access_token=response.data.access_token;
				 $scope.Token.refresh_token=response.data.refresh_token;
				 // should trigger the event to the welcome login page
				 console.log("New recieved Token is ");
				 console.log($scope.Token)
			},function errorCallback(error){
				console.log("Inside error function......")
			})
		}
	
	}])
	
	app.factory('Token', function(){
		return { access_token: '',
				 refresh_token: ''
		};
	});
	
	
	
	
	angular.module("app").controller("mainForm",['$scope','$location','$http','Token',function($scope,$location,$http,Token){
		$scope.badCredentials=false;	
		$scope.Token=Token;
		$scope.login=function(){
				if($scope.userName==='aashay' && $scope.password==='abc123'){
						$scope.badCredentials=false;
						console.log($scope.userName);
						console.log($scope.password);
						//var auth = $base64.encode($scope.userName+":"+$scope.password);
						var url="http://localhost:8080/SpringSecurityOAuth2Example/oauth/token?grant_type=password&username=bill&password=abc123";
						// Encode the String
						var string = btoa($scope.userName+":"+$scope.password);
						
						var auth=btoa("my-trusted-client:secret");
						console.log(auth);
						headers = {"Authorization": "Basic " + auth};
	
					    $http.post(url, {headers: headers}).then(function successCallback(response) {
						 console.log(response);
						 console.log(response.data.access_token);
						 $scope.Token.access_token=response.data.access_token;
						 $scope.Token.refresh_token=response.data.refresh_token;
						 // should trigger the event to the welcome login page
						 $location.path('\loggedIn');
					},function errorCallback(error){
						console.log("Inside error function......")
					})
				}
				else{
					$scope.badCredentials=true;
					$scope.userName="";
					$scope.password="";
					console.log("Credentials are not right !!!!");
				}
			}
	}])
	
	
	app.config(function($routeProvider) {
    $routeProvider
    .when("/loggedIn", {
    	//templateUrl : "loginPage.html"
    	template : "Hiiiiiii"
    })
    .when("/", {
			templateUrl : "loginPage.html"
    });
});